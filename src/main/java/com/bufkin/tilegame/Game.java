package com.bufkin.tilegame;

import com.bufkin.display.Display;

import java.awt.*;
import java.awt.image.BufferStrategy;

public class Game implements Runnable {
  private Display display;

  public final int width;
  public final int height;
  public String title;

  private boolean running = false;
  private Thread thread;

  private BufferStrategy bs;
  private Graphics g;

  public Game(String title, int width, int height) {
    this.width = width;
    this.height = height;
    this.title = title;
  }

  private void tick() {
  }

  private void render() {
    this.bs = this.display.getCanvas().getBufferStrategy();

    if (this.bs == null) {
      this.display.getCanvas().createBufferStrategy(3);
      return;
    }

    this.g = this.bs.getDrawGraphics();

    // Draw here!
    this.g.fillRect(0, 0, this.width, this.height);

    // End drawing!
    this.bs.show();
    this.g.dispose();
  }

  @Override
  public void run() {
    this.init();

    while (this.running) {
      this.tick();
      this.render();
    }

    this.stop();
  }

  private void init() {
    this.display = new Display(this.title, this.width, this.height);
  }

  public synchronized void start() {
    if (this.running) return;
    this.running = true;
    this.thread = new Thread(this);
    this.thread.start();
  }

  public synchronized void stop() {
    if (!this.running) return;
    this.running = false;
    try {
      this.thread.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
