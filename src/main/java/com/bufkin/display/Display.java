package com.bufkin.display;

import javax.swing.*;
import java.awt.*;

public class Display {
  private JFrame frame;
  private Canvas canvas;

  private final String title;
  private final int width;
  private final int height;

  public Display(final String title, final int width, final int height) {
    this.title = title;
    this.width = width;
    this.height = height;

    this.createDisplay();
  }

  private void createDisplay() {
    this.frame = new JFrame(this.title);
    this.frame.setSize(this.width, this.height);
    this.frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    this.frame.setResizable(false);
    this.frame.setLocationRelativeTo(null);
    this.frame.setVisible(true);

    this.canvas = new Canvas();
    this.canvas.setPreferredSize(new Dimension(this.width, this.height));
    this.canvas.setMaximumSize(new Dimension(this.width, this.height));
    this.canvas.setMinimumSize(new Dimension(this.width, this.height));

    this.frame.add(this.canvas);
    this.frame.pack();
  }

  public JFrame getFrame() {
    return this.frame;
  }

  public void setFrame(JFrame frame) {
    this.frame = frame;
  }

  public Canvas getCanvas() {
    return this.canvas;
  }

  public void setCanvas(Canvas canvas) {
    this.canvas = canvas;
  }

  public String getTitle() {
    return this.title;
  }

  public int getWidth() {
    return this.width;
  }

  public int getHeight() {
    return this.height;
  }
}
